data = csvread('mra_map3.csv');

figure(1);
hold on;
data(data==-1) = 100;
data(data == 20) = 50;
R=imrotate(data,270);
R = flipdim(R,2);
%R = flipdim(data,2);

I=image(R,'CDataMapping','scaled');

axis equal;
grid on;
grid minor;
hold on;
xlabel('column');
ylabel('row');
